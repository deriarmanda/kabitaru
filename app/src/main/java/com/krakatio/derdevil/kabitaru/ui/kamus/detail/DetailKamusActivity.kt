package com.krakatio.derdevil.kabitaru.ui.kamus.detail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.halilibo.bvpkotlin.BetterVideoPlayer
import com.halilibo.bvpkotlin.VideoCallback
import com.krakatio.derdevil.kabitaru.R
import com.krakatio.derdevil.kabitaru.data.model.Kamus
import kotlinx.android.synthetic.main.activity_detail_kamus.*

class DetailKamusActivity : AppCompatActivity() {

    companion object {
        private const val EXTRA_KAMUS = "extra_kamus"
        fun intent(context: Context, kamus: Kamus): Intent {
            val intent = Intent(context, DetailKamusActivity::class.java)
            intent.putExtra(EXTRA_KAMUS, kamus)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_kamus)

        val kamus = intent.getParcelableExtra<Kamus>(EXTRA_KAMUS)
        toolbar.title = kamus.textEnglish
        toolbar.subtitle = kamus.textIndo
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        text_english.text = kamus.textEnglish
        text_spelling.text = String.format("( %s )", kamus.textSpelling)
        image_kamus.setImageResource(kamus.imageRes)

        player_primary.setSource(Uri.parse("android.resource://" + packageName + "/" + kamus.primaryVideoRes))
        player_secondary.setSource(Uri.parse("android.resource://" + packageName + "/" + kamus.secondaryVideoRes))

        player_primary.setCallback(object : VideoCallback {
            override fun onPrepared(player: BetterVideoPlayer) {
                player.setVolume(0f, 0f)
            }

            override fun onBuffering(percent: Int) {}
            override fun onCompletion(player: BetterVideoPlayer) {}
            override fun onError(player: BetterVideoPlayer, e: Exception) {}
            override fun onPaused(player: BetterVideoPlayer) {}
            override fun onPreparing(player: BetterVideoPlayer) {}
            override fun onStarted(player: BetterVideoPlayer) {}
            override fun onToggleControls(player: BetterVideoPlayer, isShowing: Boolean) {}
        })
        player_secondary.setCallback(object : VideoCallback {
            override fun onPrepared(player: BetterVideoPlayer) {
                player.setVolume(0f, 0f)
            }

            override fun onBuffering(percent: Int) {}
            override fun onCompletion(player: BetterVideoPlayer) {}
            override fun onError(player: BetterVideoPlayer, e: Exception) {}
            override fun onPaused(player: BetterVideoPlayer) {}
            override fun onPreparing(player: BetterVideoPlayer) {}
            override fun onStarted(player: BetterVideoPlayer) {}
            override fun onToggleControls(player: BetterVideoPlayer, isShowing: Boolean) {}
        })
        /*player_secondary.setOnClickListener {
            Toast.makeText(
                this,
                "Fitur fullscreen belum tersedia.",
                Toast.LENGTH_LONG
            ).show()
        }*/
    }

    override fun onResume() {
        super.onResume()
        player_primary.start()
        player_secondary.start()
    }

    override fun onPause() {
        super.onPause()
        player_primary.pause()
        player_secondary.pause()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }
}
