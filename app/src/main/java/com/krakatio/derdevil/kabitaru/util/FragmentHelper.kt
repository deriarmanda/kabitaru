package com.krakatio.derdevil.kabitaru.util

import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment

const val TAG_HOME = "tag_home"
const val TAG_KAMUS = "tag_kamus"
const val TAG_KUIS = "tag_kuis"
const val TAG_INFO = "tag_informasi"
const val TAG_SEARCH = "tag_search"

fun AppCompatActivity.replaceFragmentByTag(@IdRes containerId: Int, fragment: Fragment, tag: String) {
    supportFragmentManager.beginTransaction()
        .replace(containerId, fragment, tag)
        .commit()
}