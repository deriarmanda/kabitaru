package com.krakatio.derdevil.kabitaru.ui.kuis

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.krakatio.derdevil.kabitaru.R
import com.krakatio.derdevil.kabitaru.ui.kuis.detail.DetailKuisActivity
import kotlinx.android.synthetic.main.fragment_kuis.*

class KuisFragment : Fragment() {

    companion object {
        fun newInstance() = KuisFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_kuis, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        button_mulai.setOnClickListener { startActivity(DetailKuisActivity.intent(requireContext())) }
    }
}
