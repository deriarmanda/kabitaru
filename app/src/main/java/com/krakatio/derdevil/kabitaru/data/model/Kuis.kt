package com.krakatio.derdevil.kabitaru.data.model

import androidx.annotation.RawRes

data class Kuis(
    val question: String,
    @RawRes val videoRes: Int,
    val key: String,
    val answerA: String,
    val answerB: String,
    val answerC: String,
    val answerD: String,
    var answer: String = ""
)