package com.krakatio.derdevil.kabitaru.ui.kamus.list

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.krakatio.derdevil.kabitaru.R
import com.krakatio.derdevil.kabitaru.data.model.Kamus
import com.krakatio.derdevil.kabitaru.data.model.Kategori
import com.krakatio.derdevil.kabitaru.ui.kamus.detail.DetailKamusActivity
import kotlinx.android.synthetic.main.activity_list.*

class ListActivity : AppCompatActivity() {

    private val rvAdapter = ListRvAdapter { kamus -> gotoDetailPage(kamus) }

    companion object {
        private const val EXTRA_KATEGORI = "extra_kategori"
        fun intent(context: Context, kategori: Kategori): Intent {
            val intent = Intent(context, ListActivity::class.java)
            intent.putExtra(EXTRA_KATEGORI, kategori)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list)

        val kategori = intent.getParcelableExtra<Kategori>(EXTRA_KATEGORI)
        supportActionBar?.title = kategori.title
        list_kamus.adapter = rvAdapter
        rvAdapter.list = kategori.listKamus
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return if (item?.itemId == android.R.id.home) {
            onBackPressed()
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun gotoDetailPage(kamus: Kamus) {
        startActivity(DetailKamusActivity.intent(this, kamus))
    }
}
