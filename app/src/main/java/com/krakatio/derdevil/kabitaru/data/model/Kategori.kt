package com.krakatio.derdevil.kabitaru.data.model

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.DrawableRes

data class Kategori(
    val title: String,
    @DrawableRes val imageRes: Int,
    val listKamus: List<Kamus>
) : Parcelable {
    constructor(source: Parcel) : this(
        source.readString() ?: "",
        source.readInt(),
        source.createTypedArrayList(Kamus.CREATOR) ?: ArrayList()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(title)
        writeInt(imageRes)
        writeTypedList(listKamus)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Kategori> = object : Parcelable.Creator<Kategori> {
            override fun createFromParcel(source: Parcel): Kategori = Kategori(source)
            override fun newArray(size: Int): Array<Kategori?> = arrayOfNulls(size)
        }
    }
}