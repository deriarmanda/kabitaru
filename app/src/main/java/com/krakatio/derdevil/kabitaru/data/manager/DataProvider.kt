package com.krakatio.derdevil.kabitaru.data.manager

import com.krakatio.derdevil.kabitaru.R
import com.krakatio.derdevil.kabitaru.data.model.Kamus
import com.krakatio.derdevil.kabitaru.data.model.Kategori
import com.krakatio.derdevil.kabitaru.data.model.Kuis

object DataProvider {

    fun provideKamus(): List<Kategori> {
        val listGreeting = arrayListOf(
            Kamus(
                "Good morning",
                "Selamat pagi",
                "Gud morning",
                R.drawable.ic_good_morning_64dp,
                R.raw.primary_good_morning,
                R.raw.secondary_good_morning
            ),
            Kamus(
                "Good afternoon",
                "Selamat siang",
                "Gud afternun",
                R.drawable.ic_good_afternoon_64dp,
                R.raw.primary_good_afternoon,
                R.raw.secondary_good_afternoon
            ),
            Kamus(
                "Good evening",
                "Selamat sore",
                "Gud ivening",
                R.drawable.ic_good_evening_64dp,
                R.raw.primary_good_evening,
                R.raw.secondary_good_evening
            ),
            Kamus(
                "Nice to meet you",
                "Senang bertemu denganmu",
                "Nais tu mit yu",
                R.drawable.ic_nicetomeetu_64dp,
                R.raw.primary_nice_to_meet_you,
                R.raw.secondary_nice_to_meet_you
            )
            /*Kamus(
                "How are you?",
                "Bagaimana Kabarmu?",
                "Hau ar yu?",
                R.drawable.ic_howareu_64dp,
                R.raw.big_buck_bunny,
                R.raw.big_buck_bunny
            )*/
        )
        val listGoodbye = arrayListOf(
            Kamus(
                "Goodbye",
                "Selamat tinggal",
                "Gud bai",
                R.drawable.img_goodbye,
                R.raw.primary_good_bye,
                R.raw.secondary_good_bye
            ),
            Kamus(
                "See you",
                "Sampai jumpa",
                "Si yu",
                R.drawable.ic_seeyou_64dp,
                R.raw.primary_see_you,
                R.raw.secondary_see_you
            ),
            Kamus(
                "Good night",
                "Selamat tidur",
                "Gud nait",
                R.drawable.ic_good_night_64dp,
                R.raw.primary_good_night,
                R.raw.secondary_good_night
            ),
            Kamus(
                "Bye..",
                "Dah..",
                "bai",
                R.drawable.img_goodbye,
                R.raw.primary_bye,
                R.raw.secondary_bye
            )
        )
        val listApologize = arrayListOf(
            Kamus(
                "I am sorry",
                "Mohon maaf",
                "Ai em sori",
                R.drawable.ic_imsorry_64dp,
                R.raw.primary_im_sorry,
                R.raw.secondary_im_sorry
            ),
            Kamus(
                "Please forgive me",
                "Tolong maafkan saya",
                "Plis forgiv mi",
                R.drawable.ic_pleaseforgiveme_64dp,
                R.raw.primary_please_forgive_me,
                R.raw.secondary_please_forgive_me
            ),
            Kamus(
                "Pardon me",
                "Maafkan saya",
                "Perden mi",
                R.drawable.ic_imsorry_64dp,
                R.raw.primary_pardon_me,
                R.raw.secondary_pardon_me
            ),
            Kamus(
                "It is all my fault",
                "Ini semua salahku",
                "It is ol mai folt",
                R.drawable.ic_itsallmyfault_64dp,
                R.raw.primary_its_all_my_fault,
                R.raw.secondary_its_all_my_fault
            )
        )
        val listHouse = arrayListOf(
            Kamus(
                "Curtain",
                "Gorden",
                "Kertein",
                R.drawable.ic_android_64dp,
                R.raw.primary_curtain,
                R.raw.primary_curtain
            ),
            Kamus(
                "Television",
                "Televisi",
                "Televisyen",
                R.drawable.ic_android_64dp,
                R.raw.primary_tv,
                R.raw.primary_tv
            )
        )
        val listKitchen = arrayListOf(
            Kamus(
                "Bowl",
                "Mangkuk",
                "Bowel",
                R.drawable.ic_android_64dp,
                R.raw.primary_bowl,
                R.raw.primary_bowl
            ),
            Kamus(
                "Glass",
                "Gelas",
                "Gless",
                R.drawable.ic_android_64dp,
                R.raw.primary_glass,
                R.raw.primary_glass
            ),
            Kamus(
                "Cup",
                "Cangkir",
                "Kap",
                R.drawable.ic_android_64dp,
                R.raw.primary_cup,
                R.raw.primary_cup
            ),
            Kamus(
                "Tea Pot",
                "Teko",
                "Ti pot",
                R.drawable.ic_android_64dp,
                R.raw.primary_tea_pot,
                R.raw.primary_tea_pot
            ),
            Kamus(
                "Gas Stove",
                "Kompor Gas",
                "Ges stouv",
                R.drawable.ic_android_64dp,
                R.raw.primary_gas_stove,
                R.raw.primary_gas_stove
            ),
            Kamus(
                "Frying Pan",
                "Wajan",
                "Fraying pen",
                R.drawable.ic_android_64dp,
                R.raw.primary_fraying_pan,
                R.raw.primary_fraying_pan
            ),
            Kamus(
                "Knife",
                "Pisau",
                "Naif",
                R.drawable.ic_android_64dp,
                R.raw.primary_cup,
                R.raw.primary_cup
            )
        )
        val listBedroom = arrayListOf(
            Kamus(
                "Bed",
                "Tempat Tidur",
                "Bet",
                R.drawable.ic_android_64dp,
                R.raw.primary_bed,
                R.raw.primary_bed
            ),
            Kamus(
                "Blanket",
                "Selimut",
                "Blanket",
                R.drawable.ic_android_64dp,
                R.raw.primary_blanket,
                R.raw.primary_blanket
            ),
            Kamus(
                "Clock",
                "Jam",
                "Klok",
                R.drawable.ic_android_64dp,
                R.raw.primary_clock,
                R.raw.primary_clock
            )
        )
        val listBathroom = arrayListOf(
            Kamus(
                "Towel",
                "Handuk",
                "Towel",
                R.drawable.ic_android_64dp,
                R.raw.primary_towel,
                R.raw.primary_towel
            ),
            Kamus(
                "Toilet",
                "Kloset",
                "Toilet",
                R.drawable.ic_android_64dp,
                R.raw.primary_toilet,
                R.raw.primary_toilet
            )
        )
        val listHouseYard = arrayListOf(
            Kamus(
                "Flower",
                "Bunga",
                "Flawer",
                R.drawable.ic_android_64dp,
                R.raw.primary_flower,
                R.raw.primary_flower
            ),
            Kamus(
                "Bench",
                "Bangku",
                "Benc",
                R.drawable.ic_android_64dp,
                R.raw.primary_bench,
                R.raw.primary_bench
            ),
            Kamus(
                "Grass",
                "Rumput",
                "Gres",
                R.drawable.ic_android_64dp,
                R.raw.primary_grass,
                R.raw.primary_grass
            ),
            Kamus(
                "Doormat",
                "Keset",
                "Dormat",
                R.drawable.ic_android_64dp,
                R.raw.primary_doormat,
                R.raw.primary_doormat
            )
        )
        val listGarage = arrayListOf(
            Kamus(
                "Car",
                "Mobil",
                "Kar",
                R.drawable.ic_android_64dp,
                R.raw.primary_car,
                R.raw.primary_car
            ),
            Kamus(
                "Bicycle",
                "Sepeda",
                "Baisikel",
                R.drawable.ic_android_64dp,
                R.raw.primary_bicycle,
                R.raw.primary_bicycle
            ),
            Kamus(
                "Gloves",
                "Sarung Tangan",
                "Glav",
                R.drawable.ic_android_64dp,
                R.raw.primary_gloves,
                R.raw.primary_gloves
            ),
            Kamus(
                "Broom",
                "Sapu",
                "Brum",
                R.drawable.ic_android_64dp,
                R.raw.primary_broom,
                R.raw.primary_broom
            ),
            Kamus(
                "Tools",
                "Perkakas, peralatan",
                "Tuls",
                R.drawable.ic_android_64dp,
                R.raw.primary_tools,
                R.raw.primary_tools
            )
        )
        val listClassroom = arrayListOf(
            Kamus(
                "Whiteboard",
                "Papan tulis putih",
                "Wait bod",
                R.drawable.ic_android_64dp,
                R.raw.primary_white_board,
                R.raw.primary_white_board
            ),
            Kamus(
                "Blackboard",
                "Papan tulis hitam",
                "Blek bod",
                R.drawable.ic_android_64dp,
                R.raw.primary_black_board,
                R.raw.primary_black_board
            ),
            Kamus(
                "Chalk",
                "Kapur",
                "Chak",
                R.drawable.ic_android_64dp,
                R.raw.primary_chalk,
                R.raw.primary_chalk
            ),
            Kamus(
                "Map",
                "Peta",
                "Mep",
                R.drawable.ic_android_64dp,
                R.raw.primary_map,
                R.raw.primary_map
            ),
            Kamus(
                "Window",
                "Jendela",
                "Windaw",
                R.drawable.ic_android_64dp,
                R.raw.primary_window,
                R.raw.primary_window
            ),
            Kamus(
                "Door",
                "Pintu",
                "Dor",
                R.drawable.ic_android_64dp,
                R.raw.primary_door,
                R.raw.primary_door
            ),
            Kamus(
                "Calendar",
                "Kalender",
                "Kalender",
                R.drawable.ic_android_64dp,
                R.raw.primary_calendar,
                R.raw.primary_calendar
            ),
            Kamus(
                "Vase",
                "Vas bunga",
                "Veis",
                R.drawable.ic_android_64dp,
                R.raw.primary_vase,
                R.raw.primary_vase
            ),
            Kamus(
                "Marker",
                "Spidol",
                "Marker",
                R.drawable.ic_android_64dp,
                R.raw.primary_marker,
                R.raw.primary_marker
            )
        )
        val listUniform = arrayListOf(
            Kamus(
                "Belt",
                "Ikat Pinggang",
                "Belt",
                R.drawable.ic_android_64dp,
                R.raw.primary_belt,
                R.raw.primary_belt
            )
        )

        return arrayListOf(
            Kategori("Greetings", R.drawable.ic_greeting_48dp, listGreeting),
            Kategori("Saying Goodbye", R.drawable.ic_saying_goodbye_48dp, listGoodbye),
            Kategori("Appologizing", R.drawable.ic_appologizing_48dp, listApologize),
            Kategori("House", R.drawable.ic_home, listHouse),
            Kategori("Kitchen", R.drawable.ic_kitchen, listKitchen),
            Kategori("Bedroom", R.drawable.ic_bedroom, listBedroom),
            Kategori("Bathroom", R.drawable.ic_bathroom, listBathroom),
            Kategori("House Yard", R.drawable.ic_yard, listHouseYard),
            Kategori("Garage", R.drawable.ic_garage, listGarage),
            Kategori("Classroom", R.drawable.ic_classroom, listClassroom),
            Kategori("Uniform", R.drawable.ic_uniform, listUniform)
        )
    }

    fun provideKuis(): List<Kuis> {
        return arrayListOf(
            Kuis(
                "Apa bahasa Inggris dari isyarat di atas?",
                R.raw.secondary_good_morning,
                "B",
                "Goodbye",
                "Good morning",
                "Good night",
                "I am sorry"
            ),
            Kuis(
                "Bagaimana ejaan sederhana isyarat di atas?",
                R.raw.secondary_good_afternoon,
                "C",
                "Gud nait",
                "Gud bai",
                "Gud afternun",
                "Forgiv mi"
            ),
            Kuis(
                "Apa bahasa Inggris dari isyarat di atas?",
                R.raw.secondary_good_evening,
                "B",
                "Good night",
                "Good evening",
                "Good bye",
                "Good morning"
            ),
            Kuis(
                "Bagaimana ejaan sederhana isyarat di atas?",
                R.raw.secondary_nice_to_meet_you,
                "B",
                "Gud ivening",
                "Nais to mit you",
                "Si yu",
                "Gud bai"
            ),
            Kuis(
                "Apa bahasa Inggris dari isyarat di atas?",
                R.raw.secondary_good_bye,
                "A",
                "Goodbye",
                "Good evening",
                "I am sorry",
                "Pardon me"
            ),
            Kuis(
                "Bagaimana ejaan sederhana isyarat di atas?",
                R.raw.secondary_see_you,
                "B",
                "Gud nait",
                "Si yu",
                "Bai",
                "Gud Ivening"
            ),
            Kuis(
                "Apa bahasa Inggris dari isyarat di atas?",
                R.raw.secondary_good_night,
                "C",
                "Good bye",
                "Please forgive me",
                "Good Night",
                "Pardon me"
            ),
            Kuis(
                "Bagaimana ejaan sederhana isyarat di atas?",
                R.raw.secondary_bye,
                "D",
                "Gud nait",
                "I em sori",
                "Gud ivening",
                "Bai"
            ),
            Kuis(
                "Apa bahasa Inggris dari isyarat di atas?",
                R.raw.secondary_im_sorry,
                "A",
                "I am sorry",
                "Good night",
                "That is all my fault",
                "Good morning"
            ),
            Kuis(
                "Bagaimana ejaan sederhana isyarat di atas?",
                R.raw.secondary_please_forgive_me,
                "C",
                "Gud nait",
                "I em sori",
                "Plis forgiv mi",
                "Bai"
            ),
            Kuis(
                "Apa bahasa Inggris dari isyarat di atas?",
                R.raw.secondary_pardon_me,
                "B",
                "I am sorry",
                "Pardon me",
                "That is all my fault",
                "Good morning"
            ),
            Kuis(
                "Bagaimana ejaan sederhana isyarat di atas?",
                R.raw.secondary_its_all_my_fault,
                "D",
                "Gud nait",
                "I em sori",
                "Plis forgiv mi",
                "It is ol mai folt"
            )
        )
    }
}