package com.krakatio.derdevil.kabitaru.ui.kuis.detail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.halilibo.bvpkotlin.BetterVideoPlayer
import com.halilibo.bvpkotlin.VideoCallback
import com.krakatio.derdevil.kabitaru.R
import com.krakatio.derdevil.kabitaru.data.manager.DataProvider
import com.krakatio.derdevil.kabitaru.data.model.Kuis
import com.shashank.sony.fancygifdialoglib.FancyGifDialog
import kotlinx.android.synthetic.main.activity_detail_kuis.*


class DetailKuisActivity : AppCompatActivity() {

    private val kuisList = ArrayList<Kuis>()
    private var position = -1

    companion object {
        fun intent(context: Context) = Intent(context, DetailKuisActivity::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_kuis)

        with(view_flipper) {
            isAutoStart = false
            setFlipInterval(500)
        }
        video_question_1.setCallback(object : VideoCallback {
            override fun onPrepared(player: BetterVideoPlayer) {
                player.setVolume(0f, 0f)
            }

            override fun onBuffering(percent: Int) {}
            override fun onCompletion(player: BetterVideoPlayer) {}
            override fun onError(player: BetterVideoPlayer, e: Exception) {}
            override fun onPaused(player: BetterVideoPlayer) {}
            override fun onPreparing(player: BetterVideoPlayer) {}
            override fun onStarted(player: BetterVideoPlayer) {}
            override fun onToggleControls(player: BetterVideoPlayer, isShowing: Boolean) {}
        })
        video_question_2.setCallback(object : VideoCallback {
            override fun onPrepared(player: BetterVideoPlayer) {
                player.setVolume(0f, 0f)
            }

            override fun onBuffering(percent: Int) {}
            override fun onCompletion(player: BetterVideoPlayer) {}
            override fun onError(player: BetterVideoPlayer, e: Exception) {}
            override fun onPaused(player: BetterVideoPlayer) {}
            override fun onPreparing(player: BetterVideoPlayer) {}
            override fun onStarted(player: BetterVideoPlayer) {}
            override fun onToggleControls(player: BetterVideoPlayer, isShowing: Boolean) {}
        })

        prepareKuis()
        showNextKuis()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.detail_kuis, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.next_kuis -> {
                showNextKuis()
                true
            }
            R.id.prev_kuis -> {
                showPrevKuis()
                true
            }
            R.id.finish -> {
                countAndShowScore()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun prepareKuis() {
        val list = DataProvider.provideKuis()
        kuisList.clear()
        kuisList.addAll(list)

        answer_a_1.setOnClickListener {
            enableAllAnswerButton(1)
            answer_a_1.isEnabled = false
            kuisList[position].answer = "A"
            showNextKuis()
        }
        answer_b_1.setOnClickListener {
            enableAllAnswerButton(1)
            answer_b_1.isEnabled = false
            kuisList[position].answer = "B"
            showNextKuis()
        }
        answer_c_1.setOnClickListener {
            enableAllAnswerButton(1)
            answer_c_1.isEnabled = false
            kuisList[position].answer = "C"
            showNextKuis()
        }
        answer_d_1.setOnClickListener {
            enableAllAnswerButton(1)
            answer_d_1.isEnabled = false
            kuisList[position].answer = "D"
            showNextKuis()
        }

        answer_a_2.setOnClickListener {
            enableAllAnswerButton(2)
            answer_a_2.isEnabled = false
            kuisList[position].answer = "A"
            showNextKuis()
        }
        answer_b_2.setOnClickListener {
            enableAllAnswerButton(2)
            answer_b_2.isEnabled = false
            kuisList[position].answer = "B"
            showNextKuis()
        }
        answer_c_2.setOnClickListener {
            enableAllAnswerButton(2)
            answer_c_2.isEnabled = false
            kuisList[position].answer = "C"
            showNextKuis()
        }
        answer_d_2.setOnClickListener {
            enableAllAnswerButton(2)
            answer_d_2.isEnabled = false
            kuisList[position].answer = "D"
            showNextKuis()
        }
    }

    private fun showNextKuis() {
        if (position + 1 >= kuisList.size) return //countAndShowScore()
        else {
            val kuis = kuisList[++position]
            text_number.text = String.format("%d/%d", (position + 1), kuisList.size)

            if (position % 2 == 0) {
                // Index Genap
                video_question_2.reset()
                video_question_2.setSource(Uri.parse("android.resource://" + packageName + "/" + kuis.videoRes))
                text_question_2.text = kuis.question
                answer_a_2.text = kuis.answerA
                answer_b_2.text = kuis.answerB
                answer_c_2.text = kuis.answerC
                answer_d_2.text = kuis.answerD

                enableAllAnswerButton(2)
                when (kuis.answer) {
                    "A" -> answer_a_2.isEnabled = false
                    "B" -> answer_b_2.isEnabled = false
                    "C" -> answer_c_2.isEnabled = false
                    "D" -> answer_d_2.isEnabled = false
                }
            } else {
                // Index Ganjil
                video_question_1.reset()
                video_question_1.setSource(Uri.parse("android.resource://" + packageName + "/" + kuis.videoRes))
                text_question_1.text = kuis.question
                answer_a_1.text = kuis.answerA
                answer_b_1.text = kuis.answerB
                answer_c_1.text = kuis.answerC
                answer_d_1.text = kuis.answerD

                enableAllAnswerButton(1)
                when (kuis.answer) {
                    "A" -> answer_a_1.isEnabled = false
                    "B" -> answer_b_1.isEnabled = false
                    "C" -> answer_c_1.isEnabled = false
                    "D" -> answer_d_1.isEnabled = false
                }
            }

            view_flipper.setInAnimation(this, R.anim.in_from_bottom)
            view_flipper.setOutAnimation(this, R.anim.out_to_top)
            view_flipper.showNext()
        }
    }

    private fun showPrevKuis() {
        if (position - 1 >= 0) {
            val kuis = kuisList[--position]
            text_number.text = String.format("%d/%d", (position + 1), kuisList.size)

            if (position % 2 == 0) {
                // Index Genap
                video_question_2.reset()
                video_question_2.setSource(Uri.parse("android.resource://" + packageName + "/" + kuis.videoRes))
                text_question_2.text = kuis.question
                answer_a_2.text = kuis.answerA
                answer_b_2.text = kuis.answerB
                answer_c_2.text = kuis.answerC
                answer_d_2.text = kuis.answerD

                enableAllAnswerButton(2)
                when (kuis.answer) {
                    "A" -> answer_a_2.isEnabled = false
                    "B" -> answer_b_2.isEnabled = false
                    "C" -> answer_c_2.isEnabled = false
                    "D" -> answer_d_2.isEnabled = false
                }
            } else {
                // Index Ganjil
                video_question_1.reset()
                video_question_1.setSource(Uri.parse("android.resource://" + packageName + "/" + kuis.videoRes))
                text_question_1.text = kuis.question
                answer_a_1.text = kuis.answerA
                answer_b_1.text = kuis.answerB
                answer_c_1.text = kuis.answerC
                answer_d_1.text = kuis.answerD

                enableAllAnswerButton(1)
                when (kuis.answer) {
                    "A" -> answer_a_1.isEnabled = false
                    "B" -> answer_b_1.isEnabled = false
                    "C" -> answer_c_1.isEnabled = false
                    "D" -> answer_d_1.isEnabled = false
                }
            }

            view_flipper.setInAnimation(this, R.anim.in_from_top)
            view_flipper.setOutAnimation(this, R.anim.out_to_bottom)
            view_flipper.showPrevious()
        }
    }

    private fun countAndShowScore() {
        var right = 0
        var wrong = 0
        var empty = 0

        for (kuis in kuisList) {
            when {
                kuis.answer == kuis.key -> right++
                kuis.answer.isBlank() -> empty++
                else -> wrong++
            }
        }
        val score = (right * 100) / kuisList.size

        FancyGifDialog.Builder(this)
            .setTitle("Selamat !!!")
            .setMessage(
                "Kamu berhasil menyelesaikan kuis tebak isyarat :)\n" +
                        "Skor kamu $score dari 100, lanjutkan !"
            )
            .setGifResource(R.drawable.gif_quiz_finish)
            .setPositiveBtnText("Main Lagi")
            .setPositiveBtnBackground("#2196F3")
            .setNegativeBtnText("Keluar")
            .setNegativeBtnBackground("#FFC107")
            .isCancellable(false)
            .OnPositiveClicked {
                recreate()
            }
            .OnNegativeClicked {
                finish()
            }.build()

//        val detail = getString(R.string.quiz_msg_score_detail, right, wrong, empty)
//
//        startActivity(ResultActivity.getIntent(this, score, detail))
//        finish()
    }

    private fun enableAllAnswerButton(type: Int) {
        when (type) {
            1 -> {
                answer_a_1.isEnabled = true
                answer_b_1.isEnabled = true
                answer_c_1.isEnabled = true
                answer_d_1.isEnabled = true
            }
            2 -> {
                answer_a_2.isEnabled = true
                answer_b_2.isEnabled = true
                answer_c_2.isEnabled = true
                answer_d_2.isEnabled = true
            }
            else -> {
                answer_a_1.isEnabled = true
                answer_b_1.isEnabled = true
                answer_c_1.isEnabled = true
                answer_d_1.isEnabled = true

                answer_a_2.isEnabled = true
                answer_b_2.isEnabled = true
                answer_c_2.isEnabled = true
                answer_d_2.isEnabled = true
            }
        }
    }
}
