package com.krakatio.derdevil.kabitaru.ui.kamus.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.krakatio.derdevil.kabitaru.R
import com.krakatio.derdevil.kabitaru.data.model.Kamus
import kotlinx.android.synthetic.main.item_kamus.view.*

class ListRvAdapter(
    private val onItemClick: ((Kamus) -> Unit)
) : RecyclerView.Adapter<ListRvAdapter.ViewHolder>() {

    var list: List<Kamus> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_kamus, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.fetch(list[position])

    override fun getItemCount() = list.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(kamus: Kamus) {
            itemView.text_english.text = kamus.textEnglish
            itemView.text_indo.text = kamus.textIndo
            itemView.card_container.setOnClickListener { onItemClick(kamus) }
        }
    }
}