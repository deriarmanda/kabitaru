package com.krakatio.derdevil.kabitaru.ui.kamus

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.krakatio.derdevil.kabitaru.R
import com.krakatio.derdevil.kabitaru.data.model.Kategori
import kotlinx.android.synthetic.main.item_kategori.view.*

class KamusRvAdapter(
    private val onItemClick: ((Kategori) -> Unit)
) : RecyclerView.Adapter<KamusRvAdapter.ViewHolder>() {

    var list: List<Kategori> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_kategori, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.fetch(list[position])

    override fun getItemCount() = list.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(kategori: Kategori) {
            itemView.text_kategori.text = kategori.title
            itemView.text_kategori.setCompoundDrawablesWithIntrinsicBounds(0, kategori.imageRes, 0, 0)
            itemView.card_container.setOnClickListener { onItemClick(kategori) }
        }
    }
}