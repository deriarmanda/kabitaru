package com.krakatio.derdevil.kabitaru.data.model

import android.os.Parcel
import android.os.Parcelable
import androidx.annotation.DrawableRes
import androidx.annotation.RawRes

data class Kamus(
    val textEnglish: String,
    val textIndo: String,
    val textSpelling: String,
    @DrawableRes val imageRes: Int,
    @RawRes val primaryVideoRes: Int,
    @RawRes val secondaryVideoRes: Int
) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readString() ?: "",
        parcel.readInt(),
        parcel.readInt(),
        parcel.readInt()
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(textEnglish)
        parcel.writeString(textIndo)
        parcel.writeString(textSpelling)
        parcel.writeInt(imageRes)
        parcel.writeInt(primaryVideoRes)
        parcel.writeInt(secondaryVideoRes)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Kamus> {
        override fun createFromParcel(parcel: Parcel): Kamus {
            return Kamus(parcel)
        }

        override fun newArray(size: Int): Array<Kamus?> {
            return arrayOfNulls(size)
        }
    }
}