package com.krakatio.derdevil.kabitaru.ui.kamus

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.krakatio.derdevil.kabitaru.data.manager.DataProvider
import com.krakatio.derdevil.kabitaru.data.model.Kategori

class KamusViewModel : ViewModel() {

    private val listKategori = MutableLiveData<List<Kategori>>()

    init {
        listKategori.value = DataProvider.provideKamus()
    }

    fun getListKategori(): LiveData<List<Kategori>> = listKategori
}
