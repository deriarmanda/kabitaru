package com.krakatio.derdevil.kabitaru.ui.search

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.krakatio.derdevil.kabitaru.R
import com.krakatio.derdevil.kabitaru.data.model.Kamus
import kotlinx.android.synthetic.main.item_search.view.*

class SearchAdapter(
    private val onClick: (kamus: Kamus) -> Unit
) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {

    private var kategoriList = listOf<String>()
    private var kamusList = listOf<Kamus>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_search, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = kamusList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.fetch(kamusList[position])
    }

    fun setResult(kategoriList: List<String>, kamusList: List<Kamus>) {
        this.kategoriList = kategoriList
        this.kamusList = kamusList
        notifyDataSetChanged()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun fetch(kamus: Kamus) {
            itemView.text_english.text = kamus.textEnglish
            itemView.text_indo.text = kamus.textIndo
            itemView.card_container.setOnClickListener { onClick(kamus) }

            val headerVisible =
                adapterPosition == 0 || (kategoriList[adapterPosition] != kategoriList[adapterPosition - 1])
            itemView.text_kategori.visibility = if (headerVisible) View.VISIBLE else View.GONE
            itemView.text_kategori.text = kategoriList[adapterPosition]
        }
    }
}