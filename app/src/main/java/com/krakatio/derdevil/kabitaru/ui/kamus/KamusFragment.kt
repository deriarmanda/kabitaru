package com.krakatio.derdevil.kabitaru.ui.kamus

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.krakatio.derdevil.kabitaru.R
import com.krakatio.derdevil.kabitaru.data.model.Kategori
import com.krakatio.derdevil.kabitaru.ui.kamus.list.ListActivity
import kotlinx.android.synthetic.main.fragment_kamus.*

class KamusFragment : Fragment() {

    private val rvAdapter = KamusRvAdapter { kategori -> gotoListKamusPage(kategori) }
    private lateinit var viewModel: KamusViewModel

    companion object {
        fun newInstance() = KamusFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_kamus, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(requireActivity()).get(KamusViewModel::class.java)
        viewModel.getListKategori().observe(viewLifecycleOwner, Observer { rvAdapter.list = it })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        list_kategori.adapter = rvAdapter
    }

    private fun gotoListKamusPage(kategori: Kategori) {
        startActivity(ListActivity.intent(requireContext(), kategori))
    }
}
