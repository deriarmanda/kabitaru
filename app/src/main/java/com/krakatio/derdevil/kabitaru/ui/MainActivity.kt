package com.krakatio.derdevil.kabitaru.ui

import android.os.Bundle
import android.view.Menu
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.krakatio.derdevil.kabitaru.R
import com.krakatio.derdevil.kabitaru.ui.home.HomeFragment
import com.krakatio.derdevil.kabitaru.ui.info.InfoFragment
import com.krakatio.derdevil.kabitaru.ui.kamus.KamusFragment
import com.krakatio.derdevil.kabitaru.ui.kuis.KuisFragment
import com.krakatio.derdevil.kabitaru.ui.search.SearchFragment
import com.krakatio.derdevil.kabitaru.util.*
import com.miguelcatalan.materialsearchview.MaterialSearchView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), SearchFragment.OnSearchCompleteListener {

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val fragment: Fragment?
        when (item.itemId) {
            R.id.navigation_home -> {
                fragment = supportFragmentManager.findFragmentByTag(TAG_HOME) ?: HomeFragment.newInstance()
                replaceFragmentByTag(R.id.container, fragment, TAG_HOME)
                search_view.visibility = View.GONE
                toolbar.visibility = View.GONE
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_kamus -> {
                fragment = supportFragmentManager.findFragmentByTag(TAG_KAMUS) ?: KamusFragment.newInstance()
                replaceFragmentByTag(R.id.container, fragment, TAG_KAMUS)
                search_view.visibility = View.VISIBLE
                toolbar.visibility = View.VISIBLE
                toolbar.title = String.format("   %s", getString(R.string.title_kamus))
                toolbar.setLogo(R.drawable.ic_book_white_24dp)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_kuis -> {
                fragment = supportFragmentManager.findFragmentByTag(TAG_KUIS) ?: KuisFragment.newInstance()
                replaceFragmentByTag(R.id.container, fragment, TAG_KUIS)
                search_view.visibility = View.VISIBLE
                toolbar.visibility = View.VISIBLE
                toolbar.title = String.format("   %s", getString(R.string.title_kuis))
                toolbar.setLogo(R.drawable.ic_assignment_white_24dp)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_info -> {
                fragment = supportFragmentManager.findFragmentByTag(TAG_INFO) ?: InfoFragment.newInstance()
                replaceFragmentByTag(R.id.container, fragment, TAG_INFO)
                search_view.visibility = View.VISIBLE
                toolbar.visibility = View.VISIBLE
                toolbar.title = String.format("   %s", getString(R.string.title_info))
                toolbar.setLogo(R.drawable.ic_info_white_24dp)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        navigation.selectedItemId = R.id.navigation_home

        search_view.setOnSearchViewListener(object : MaterialSearchView.SearchViewListener {
            override fun onSearchViewClosed() {
                closeSearchPage()
            }

            override fun onSearchViewShown() {
                openSearchPage()
            }
        })
        search_view.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                search_view.clearFocus()
                val fragment = supportFragmentManager.findFragmentByTag(TAG_SEARCH)
                return if (fragment is SearchFragment && query != null && query.isNotBlank()) {
                    fragment.processQuery(query)
                    true
                } else false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                val fragment = supportFragmentManager.findFragmentByTag(TAG_SEARCH)
                return if (fragment is SearchFragment && newText != null && newText.isNotBlank()) {
                    fragment.processQuery(newText)
                    true
                } else false
            }
        })
    }

    override fun onBackPressed() {
        when {
            search_view.isSearchOpen -> search_view.closeSearch()
            navigation.selectedItemId != R.id.navigation_home -> navigation.selectedItemId = R.id.navigation_home
            else -> super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.search_main, menu)
        search_view.setMenuItem(menu.findItem(R.id.action_search))
        return true
    }

    override fun onSearchComplete() {
        search_view.closeSearch()
    }

    private fun openSearchPage() {
        val fragment = supportFragmentManager.findFragmentByTag(TAG_SEARCH) ?: SearchFragment.newInstance()
        replaceFragmentByTag(R.id.container, fragment, TAG_SEARCH)
        navigation.visibility = View.GONE
    }

    private fun closeSearchPage() {
        navigation.selectedItemId = navigation.selectedItemId
        navigation.visibility = View.VISIBLE
    }
}
