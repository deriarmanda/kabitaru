package com.krakatio.derdevil.kabitaru.ui.search


import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.krakatio.derdevil.kabitaru.R
import com.krakatio.derdevil.kabitaru.data.manager.DataProvider
import com.krakatio.derdevil.kabitaru.data.model.Kamus
import com.krakatio.derdevil.kabitaru.ui.kamus.detail.DetailKamusActivity
import kotlinx.android.synthetic.main.fragment_search.*

/**
 * A simple [Fragment] subclass.
 *
 */
class SearchFragment : Fragment() {

    companion object {
        fun newInstance() = SearchFragment()
    }

    private var onSearchCompleteListener: OnSearchCompleteListener? = null
    private val data = DataProvider.provideKamus()
    private val adapter = SearchAdapter {
        startActivity(DetailKamusActivity.intent(requireContext(), it))
        onSearchCompleteListener?.onSearchComplete()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnSearchCompleteListener) onSearchCompleteListener = context
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_search, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recycler_view.adapter = adapter
    }

    override fun onDetach() {
        super.onDetach()
        onSearchCompleteListener = null
    }

    fun processQuery(query: String) {
        if (query.isBlank()) {
            adapter.setResult(emptyList(), emptyList())
            return
        }
        val kategori = arrayListOf<String>()
        val kamus = arrayListOf<Kamus>()
        val queryLowerCase = query.toLowerCase()
        for (datum in data) {
            for (result in datum.listKamus) {
                val english = result.textEnglish.toLowerCase()
                val indo = result.textIndo.toLowerCase()
                if (english.contains(queryLowerCase) || indo.contains(queryLowerCase)) {
                    kategori.add(datum.title)
                    kamus.add(result)
                }
            }
        }
        adapter.setResult(kategori, kamus)
    }

    interface OnSearchCompleteListener {
        fun onSearchComplete()
    }

}
